var ordPage=false;
function loadPage(id) {
    /* $.get( id+".html", function( data ) {
     $( "#popupPage" ).html(data);
     $( "#popupPage" ).show();
     // toggleNav();
     }); */
    
    $("#loading").show();
    $.ajax({
           url: ServerUrl + "readFile?file=" + id ,
           type: "GET",
           data: {},
           dataType: "json",
           contentType: "application/json; charset=utf-8",
           traditional: true,
           success: function(data) {
           $("#popupPage").html('<style>.animatedSlow {-webkit-animation-duration:0.8s;animation-duration:0.8s;-webkit-animation-fill-mode:both;animation-fill-mode:both}.close-btn{ width: 30px;height: 30px;border-radius: 50px;border: 1px solid #fff;background-color: rgba(255,255,255,0.2);position: absolute;right: 15px;top: 20px;z-index: 999; line-height:30px; text-align:center; color:#fff !important; font-weight:bold;}.close-btn:hover{ color:#000;  background-color: rgba(255,255,255,0.5); }.cross1{ width:50%; height:3px; background-color:#fff; position:relative; top:46%; left:26%; transform: rotate(45deg);}.cross2{ width:50%; height:3px; background-color:#fff; position:relative; top:36%; left:26%; transform: rotate(-45deg);}@-webkit-keyframes DDDown {0% {opacity:0;-webkit-transform:translateY(-20px);transform:translateY(-20px)}100% {opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes DDDown {0% {opacity:0;-webkit-transform:translateY(-20px);-ms-transform:translateY(-20px);transform:translateY(-20px) }100% {opacity:1;-webkit-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0)}}.DDDown {-webkit-animation-name:DDDown;animation-name:DDDown}ol{ padding:0 0 0 5px !important; color:#999;}li{ padding:0 0 0 8px !important; color:#999;}.termscontent{ color:#bbb;} .popUpCont h1, .popUpCont h2{ margin-top:10px !important; color:#fff;}.popUpCont p { color:#bbb;}.popUpCont a{ color:#af0405; text-decoration:none;}.popUpCont{ width:100%; height:100vh; position:relative; top:0px; overflow-y:scroll; margin:0 auto; padding:10px;box-sizing: border-box; background-color: #000;}</style><div  class="popUpCont"><a class="close-btn" href="javascript:closePage()">X</a>'+data.content.body+'</div>');
           $("#loading").hide();
           $(".mainwrapper").hide();
           $("#popupPage").show();
           if ($("nav").length) {
           //toggleNav();
           };
           },
           error: function(jqXHR, textStatus, errorThrown) {
           },
           complete: function() {
           
           }
           });
    
}
function loadLocalPage(id) {
    $.get( id, function( data ) {
          $("#popupPage").html('<style>.animatedSlow {-webkit-animation-duration:0.8s;animation-duration:0.8s;-webkit-animation-fill-mode:both;animation-fill-mode:both}.close-btn{ width: 30px;height: 30px;border-radius: 50px;border: 1px solid #fff;background-color: rgba(255,255,255,0.2);position: absolute;right: 15px;top: 20px;z-index: 999; line-height:30px; text-align:center; color:#fff !important; font-weight:bold;}.close-btn:hover{ color:#000;  background-color: rgba(255,255,255,0.5); }.cross1{ width:50%; height:3px; background-color:#fff; position:relative; top:46%; left:26%; transform: rotate(45deg);}.cross2{ width:50%; height:3px; background-color:#fff; position:relative; top:36%; left:26%; transform: rotate(-45deg);}@-webkit-keyframes DDDown {0% {opacity:0;-webkit-transform:translateY(-20px);transform:translateY(-20px)}100% {opacity:1;-webkit-transform:translateY(0);transform:translateY(0)}}@keyframes DDDown {0% {opacity:0;-webkit-transform:translateY(-20px);-ms-transform:translateY(-20px);transform:translateY(-20px) }100% {opacity:1;-webkit-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0)}}.DDDown {-webkit-animation-name:DDDown;animation-name:DDDown}ol{ padding:0 0 0 5px !important; color:#999;}li{ padding:0 0 0 8px !important; color:#999;}.termscontent{ color:#bbb;} .popUpCont h1, .popUpCont h2{ margin-top:10px !important; color:#fff;}.popUpCont p { color:#bbb;}.popUpCont a{ color:#af0405; text-decoration:none;}.popUpCont{ width:100%; height:100vh; position:relative; top:0px; overflow-y:scroll; margin:0 auto; padding:10px;box-sizing: border-box; background-color: #000; }</style><div id="popUpCont" class="popUpCont"><a class="close-btn" href="javascript:closePage()">X</a>'+data+'</div>');
          $("#loading").hide();
          $(".mainwrapper").hide();
          $("#popupPage").show();
          if (($("nav").length )&& id=='myAccount.html' ){
          $(".editbtn").show();
          toggleNav();
          getuserdetails();
          getdevicepaired();
          getAddress();
          }
          else if(id=='search.html')
          {
          $("#popUpCont").addClass("srch_sc")
          $("#search").focus();
          }
          // toggleNav();
          if(id=='myorders.html'){
          ordPage=true;
          
          // get orders list
          
          var myDB = window.sqlitePlugin.openDatabase({name: 'DameDashDB.db', location: 'default'}, null, null);
          myDB.transaction(function(transaction) {
                           transaction.executeSql('SELECT * FROM damedashSettings', [], function(tx, results) {
                                                  var len = results.rows.length,
                                                  i = 0;
                                                  if (len > 0) {
                                                  var NavItems = [];
                                                  var UserID = results.rows.item(i).key3;
                                                  $.ajax({
                                                         url: ServerLamdaUrl + "fetchmyordersdetails?token=" + UserID + "&appname=" + appName + "&operation=null&item=null",
                                                         type: "POST",
                                                         data: '',
                                                         dataType: "json",
                                                         contentType: "application/json; charset=utf-8",
                                                         traditional: true,
                                                         
                                                         //                            url: ServerUrl + "fetchmyOrdersdetails",
                                                         //                            type: "POST",
                                                         //                            data: JSON.stringify({
                                                         //                                "fetchmyOrdersdetails": {
                                                         //                                    "user_id": UserID,
                                                         //                                    "pageSize":"10",
                                                         //                                    "page":"1",
                                                         //                                    "operation":"initial"
                                                         //                                }
                                                         //                            }),
                                                         //                            dataType: "json",
                                                         //                            contentType: "application/json; charset=utf-8",
                                                         //                            traditional: true,
                                                         success: function(data) {
                                                         
                                                         
                                                         
                                                         if (data && data.result.length>0)
                                                         
                                                         {
                                                         for (var i = 0; i < data.result.length; i++) {
                                                         
                                                         var descId="detailsTxt_"+data.result[i].order.orderId;
                                                         var btndescId="button_"+data.result[i].order.orderId;
                                                         
                                                         //var ordHTML=" <div>Order ID :"+ data[i]["transaction"]["orderId"]+"</div>";
                                                         var ordHTML='<div class="my_ord"><div class="ordd">  <div class="ordd_prd"><span> </span><img class="ordd_img" src='+data.result[i].order.productImage+'>    <h4><span>'+data.result[i].order.productName+'</span><span> </span></h4>  </div>  <div class="ordd_inv"><p class="ordd_qty"><span>Quantity:</span><strong>'+data.result[i].order.productQuantity+'</strong><br> <span>Price </span><strong><span>'+data.result[i].order.productPrice+'</span></strong></p>    <p><span>Order ID: </span><span>'+data.result[i].order.orderId+'</span></p></div><div class="trk">    <p><span>Status: </span><strong>'+data.result[i].transaction.status+'</strong></p> <div class="ordd_dt"><span>Placed on </span><span>'+data.result[i].order.orderDate+'</span></div> <div class="od_pze"><p><span>Order Total </span><strong ><span>$</span><span>'+(data.result[i].order.productQuantity * data.result[i].order.productClearancePrice).toFixed(2)+'</span></strong></p></div>  </div><p class="hide" id='+descId+'>Details Data</p></div><div class="ordd_ft hide"><div class="ordd_bnt" id='+btndescId+' value='+descId+'  role="button">Details </div></div></div>';
                                                         $("#orderDiv").append(ordHTML);
                                                         }
                                                         }
                                                         else
                                                         {
                                                         $("#orderDiv").append("NO RECORDS FOUND");
                                                         }
                                                         
                                                         
                                                         },
                                                         error: function(jqXHR, textStatus, errorThrown) {
                                                         console.log( "Error"+textStatus);
                                                         },
                                                         complete: function() {
                                                         
                                                         }
                                                         });
                                                  } else {
                                                  showAlert('No user found.');
                                                  Logout();
                                                  }
                                                  }, null);
                           });
          
          
          
          ///////
          
          }
          else
          {
          ordPage=false;
          }
          });
    
    $("#loading").show();
    
    
}

$(document).on('click','div',function(e){
               
               var btId = $("#"+e.target.id).val();
               $("#"+btId).removeClass('hide');
               
               })

function myFunctionNew(idValue) {
    alert('id value '+JSON.stringify(idValue));
    
    /*  var x = document.getElementById('detailstxt');
     if (x.style.display === 'block') {
     x.style.display = 'none';
     } else {
     x.style.display = 'block';
     }*/
}

function closePage() {
    
    if(ordPage==true){
        
        loadLocalPage('myAccount.html');
        ordPage=false;
    } else {
        $("#popupPage").hide();
        $("#popupPage").html('');
        $(".mainwrapper").show();
    }
    
    
    
    
    /*    if ($("nav").length) {
     toggleNav();
     };*/
    
}

