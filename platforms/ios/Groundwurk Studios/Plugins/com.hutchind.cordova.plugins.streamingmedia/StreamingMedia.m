#import "StreamingMedia.h"
#import <Cordova/CDV.h>


@interface VideoPlayerController : AVPlayerViewController

@end
AVPlayer *player;
NSTimer *playstoptimer;
@implementation VideoPlayerController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self.player play];
//    CMTime time = CMTimeMakeWithSeconds(100, NSEC_PER_SEC);
//    [player seekToTime:time];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceOrientationLandscape) name:AVPlayerItemTimeJumpedNotification object:Nil];

    
}

-(BOOL)shouldAutorotate{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (void)forceOrientationLandscape
{
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight] forKey:@"orientation"];
}

@end

@interface StreamingMedia()
	- (void)parseOptions:(NSDictionary *) options type:(NSString *) type;
	- (void)play:(CDVInvokedUrlCommand *) command type:(NSString *) type;
	- (void)startPlayer:(NSString*)uri;
@end

@implementation StreamingMedia
	NSString* callbackId;
	VideoPlayerController *moviePlayer;
	BOOL shouldAutoClose;
	UIColor *backgroundColor;
	UIImageView *imageView;
    BOOL initFullscreen;
    int seekTime;
    NSString *userId;
    int  videoId;
    NSString *serverURL;





NSString * const TYPE_VIDEO = @"VIDEO";
NSString * const DEFAULT_IMAGE_SCALE = @"center";

-(void)parseOptions:(NSDictionary *)options type:(NSString *) type {
	
    if (![options isKindOfClass:[NSNull class]] && [options objectForKey:@"seekTime"]) {
        seekTime = [[options objectForKey:@"seekTime"] intValue];
    } else {
        seekTime = 0;
    }
  
    if (![options isKindOfClass:[NSNull class]] && [options objectForKey:@"userId"]) {
        userId = [options objectForKey:@"userId"];
    } else {
        userId = @"";
    }
    
    if (![options isKindOfClass:[NSNull class]] && [options objectForKey:@"videoId"]) {
        videoId = [[options objectForKey:@"videoId"] intValue];
    } else {
        videoId = 0;
    }
    
    if (![options isKindOfClass:[NSNull class]] && [options objectForKey:@"serverURL"]) {
        serverURL = [options objectForKey:@"serverURL"] ;
    } else {
        serverURL = @"";
    }
    
    // No specific options for video yet
}

-(void)play:(CDVInvokedUrlCommand *) command type:(NSString *) type {
    
	callbackId = command.callbackId;
	NSString *mediaUrl  = [command.arguments objectAtIndex:0];
	[self parseOptions:[command.arguments objectAtIndex:1] type:type];
	[self startPlayer:mediaUrl];
}

-(void)playVideo:(CDVInvokedUrlCommand *) command {
  
	[self play:command type:[NSString stringWithString:TYPE_VIDEO]];
}
-(void)stopVideo:(CDVInvokedUrlCommand *) command {
    NSLog(@"Closed Rajesh");

}

-(void)playAudio:(CDVInvokedUrlCommand *) command {
    // Method here
}

-(void)startPlayer:(NSString*)uri {
   //AVPlayer *player = [AVPlayer playerWithURL:[NSURL URLWithString:uri]];
    player = [AVPlayer playerWithURL:[NSURL URLWithString:uri]];
    moviePlayer = [[VideoPlayerController alloc] init];
    moviePlayer.player = player;
    moviePlayer.showsPlaybackControls = YES;
    player.closedCaptionDisplayEnabled = YES;
    
    [self.viewController presentViewController:moviePlayer animated:YES completion:nil];
    CMTime time = CMTimeMakeWithSeconds(seekTime, NSEC_PER_SEC);
    [player seekToTime:time];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:AVPlayerItemDidPlayToEndTimeNotification object:player.currentItem];
    playstoptimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector: @selector(playerstop:) userInfo:nil repeats:true];
    [NSTimer scheduledTimerWithTimeInterval:20.0 target:self selector: @selector(updatetime:) userInfo:nil repeats:true];
 
 }
- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    NSLog(@"Iam at play end");
}

- (void)playerstop:(NSTimer *)command
{
    if (player.rate == 0.0)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
        [UIView animateWithDuration:0.25 animations:^{
            
        }];
        playstoptimer.invalidate;
    }
    
}


-(void)updatetime:(NSTimer *) command {
    
    
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];

    
    NSURL *url = [NSURL URLWithString:serverURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"POST"];
    Float64 dur = CMTimeGetSeconds([moviePlayer.player currentTime]);

    NSDictionary *mapData = @{@"updateSeekTime":@{@"userId": userId, @"videoId":[NSString stringWithFormat:@"%d",videoId], @"seekTime":[NSString stringWithFormat:@"%f",dur]}};
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:0 error:&error];
    [request setHTTPBody:postData];
    
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
        
        if(error == nil)
        {
           // NSLog(@"%@",response);
        }
        else{//NSLog(@"%@",error.localizedDescription);
        }
        
    }];
    
    [postDataTask resume];
    
}


@end
